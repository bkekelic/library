package bkekelic.library

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import bkekelic.library.model.Book
import kotlinx.android.synthetic.main.activity_book_info.*

class BookInfoActivity : AppCompatActivity() {

    companion object {
        const val KEY_BOOK_INFO = "book_info_key"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_book_info)

        initializeUi()
        displayReceivedData()
    }

    private fun initializeUi() {
        title = getString(R.string.title_bookInfo)
    }

    private fun displayReceivedData() {
        val book = intent?.extras?.getParcelable(KEY_BOOK_INFO) as Book
        book.let {
            bookTitleView.text = it.title
            bookAuthorView.text = it.author
            bookYearView.text = it.yearOfPublish.toString()
        }
    }

}
