package bkekelic.library.model

import android.arch.persistence.room.*
import io.reactivex.Flowable

@Dao
interface BookDao {

    @Insert
    fun insertBook(book: Book)

    @Query("SELECT * FROM table_books ORDER BY yearOfPublish DESC")
    fun getAllBooks(): Flowable<List<Book>>

    @Delete
    fun deleteBook(book: Book)

    @Query("SELECT * FROM table_books WHERE id=(SELECT max(id) FROM table_books)")
    fun getBookWithMaxId(): Book?

}