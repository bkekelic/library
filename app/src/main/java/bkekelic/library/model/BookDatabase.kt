package bkekelic.library.model

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

@Database(version = 1, entities = [Book::class])
abstract class BookDatabase : RoomDatabase() {

    abstract fun bookDao(): BookDao

    companion object {
        private var instance: BookDatabase? = null

        fun getInstance(context: Context): BookDatabase {
            if (instance == null) {
                instance =
                    Room.databaseBuilder(context, BookDatabase::class.java, "book_database").allowMainThreadQueries()
                        .build()
            }
            return instance as BookDatabase
        }
    }

}