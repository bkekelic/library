package bkekelic.library.repository

import android.app.Application
import android.os.AsyncTask
import bkekelic.library.model.Book
import bkekelic.library.model.BookDao
import bkekelic.library.model.BookDatabase
import io.reactivex.Flowable

data class BookRepository(private val application: Application) {

    private var database: BookDatabase = BookDatabase.getInstance(application)
    private var bookDao: BookDao
    private var allBooks: Flowable<List<Book>>

    init {
        bookDao = database.bookDao()
        allBooks = bookDao.getAllBooks()
    }

    fun insertBook(book: Book) {
        InsertBookAsync(bookDao).execute(book)
    }

    fun getAllBooks(): Flowable<List<Book>> {
        return allBooks
    }

    fun deleteBook(book: Book) {
        DeleteBookAsync(bookDao).execute(book)
    }

    fun getBookWithMaxId(): Book? {
        return bookDao.getBookWithMaxId()
    }
}

class InsertBookAsync(private val bookDao: BookDao) : AsyncTask<Book, Void, Void>() {
    override fun doInBackground(vararg params: Book?): Void? {
        bookDao.insertBook(params[0]!!)
        return null
    }
}

class DeleteBookAsync(private val bookDao: BookDao) : AsyncTask<Book, Void, Void>() {
    override fun doInBackground(vararg params: Book?): Void? {
        bookDao.deleteBook(params[0]!!)
        return null
    }
}