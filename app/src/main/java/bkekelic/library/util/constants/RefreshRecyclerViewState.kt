package bkekelic.library.util.constants

import bkekelic.library.model.Book

data class RefreshRecyclerViewState(
    var position: Int,
    var state: RecyclerViewState,
    var book: Book
)

enum class RecyclerViewState {
    FILL_LIST,
    DELETE_ITEM,
    ADD_ITEM
}