package bkekelic.library.util.extensions

import android.content.Context
import android.util.Log
import android.widget.Toast

fun Context.toastShort(message: String){
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}
fun Context.toastShort(messageRes: Int){
    Toast.makeText(this, messageRes, Toast.LENGTH_SHORT).show()
}

fun logTAG(message: String){
    Log.d("TAG", message)
}