package bkekelic.library.view.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import bkekelic.library.R
import bkekelic.library.model.Book
import bkekelic.library.util.extensions.toastShort
import kotlinx.android.synthetic.main.activity_add_new_book.*

class AddNewBookActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_new_book)

        initializeUi()
    }

    companion object {
        const val REQUEST_NEW_BOOK: Int = 10
        const val KEY_EXTRA_NEW_BOOK: String = "new_book_key"
    }

    private fun initializeUi() {
        title = getString(R.string.title_addNewBook)
        fab_returnNewBook.setOnClickListener { returnNewBook() }
    }

    private fun returnNewBook() {
        if (bookTitleView.text.isEmpty() || bookAuthorView.text.isEmpty() || bookYearView.text.isEmpty()) {
            toastShort(getString(R.string.input_newBookError))
        } else {

            val book = Book(
                0,
                bookTitleView.text.toString(),
                bookAuthorView.text.toString(),
                bookYearView.text.toString().toInt()
            )
            val resultIntent = Intent()
            resultIntent.putExtra(KEY_EXTRA_NEW_BOOK, book)

            this.setResult(Activity.RESULT_OK, resultIntent)
            this.finish()
        }
    }
}
