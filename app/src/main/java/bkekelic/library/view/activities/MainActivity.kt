package bkekelic.library.view.activities

import android.app.Activity
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import bkekelic.library.R
import bkekelic.library.model.Book
import bkekelic.library.util.constants.RecyclerViewState
import bkekelic.library.util.constants.RefreshRecyclerViewState
import bkekelic.library.util.extensions.toastShort
import bkekelic.library.view.adapters.SwipeToDeleteCallback
import bkekelic.library.viewModel.BookViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    private lateinit var bookViewModel: BookViewModel
    private var recyclerViewState = RefreshRecyclerViewState(0, RecyclerViewState.FILL_LIST, Book(0, "", "", 0))

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initializeUi()
    }

    private fun initializeUi() {
        fab_addBook.setOnClickListener { openIntentForNewBook() }

        initViewModel()
        initRecyclerView()
    }


    private fun initViewModel() {
        bookViewModel = ViewModelProviders.of(this).get(BookViewModel::class.java)
    }

    private fun initRecyclerView() {
        bookListView.layoutManager = LinearLayoutManager(this)
        bookListView.adapter = bookViewModel.bookAdapter

        // For swipe and delete
        val swipeHandler = object : SwipeToDeleteCallback() {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, size: Int) {

                recyclerViewState.state = RecyclerViewState.DELETE_ITEM
                recyclerViewState.position = viewHolder.adapterPosition
                bookViewModel.deleteBook(
                    bookViewModel.bookAdapter.getBookByPosition(viewHolder.adapterPosition)
                )

            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(bookListView)



        subscribeToChangesInList()
    }

    private fun subscribeToChangesInList() {
        addDisposable(
            bookViewModel.getAllBooks()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        modifyRecyclerViewList(it)
                    },
                    { throwable ->
                        toastShort(throwable.localizedMessage)
                    },
                    {
                        toastShort(getString(R.string.toast_streamCompleted))
                    }
                )
        )
    }


    private fun modifyRecyclerViewList(books: List<Book>?) {
        when (recyclerViewState.state) {
            RecyclerViewState.FILL_LIST -> fillList(books)
            RecyclerViewState.DELETE_ITEM -> deleteItemInList(recyclerViewState.position)
            RecyclerViewState.ADD_ITEM -> addItemInList(recyclerViewState.position, recyclerViewState.book)
        }
    }

    private fun addItemInList(position: Int, book: Book) {
        bookViewModel.bookAdapter.addAt(position, book)
    }

    private fun deleteItemInList(position: Int) {
        bookViewModel.bookAdapter.removeAt(position)

    }

    private fun fillList(books: List<Book>?) {
        bookViewModel.bookAdapter.setBooks(books!!.toMutableList())
    }

    private fun openIntentForNewBook() {
        val addNewBookIntent = Intent(this, AddNewBookActivity::class.java)
        startActivityForResult(addNewBookIntent, AddNewBookActivity.REQUEST_NEW_BOOK)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            AddNewBookActivity.REQUEST_NEW_BOOK -> {
                if (resultCode == Activity.RESULT_OK) {

                    val newBook = data?.getParcelableExtra(AddNewBookActivity.KEY_EXTRA_NEW_BOOK) as? Book
                    if (newBook == null) toastShort(getString(R.string.error_addingNewBook))
                    else addNewBook(newBook)

                } else {
                    toastShort(getString(R.string.resultIntent_error))
                }

            }
        }
    }

    private fun addNewBook(newBook: Book) {

        var maxID = bookViewModel.getBookWithMaxId()?.id
        if (maxID == null) maxID = 0

        newBook.id = maxID + 1

        recyclerViewState = RefreshRecyclerViewState(
            bookViewModel.bookAdapter.getNewPositionOfNewBook(newBook),
            RecyclerViewState.ADD_ITEM,
            newBook
        )
        bookViewModel.insertBook(newBook)

        toastShort(getString(R.string.success_addNewBook))
    }
}
