package bkekelic.library.view.adapters

import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import bkekelic.library.R
import bkekelic.library.model.Book
import kotlinx.android.synthetic.main.item_book.view.*

class BookAdapter(bookListener: BookInteractionListener) : RecyclerView.Adapter<BookHolder>() {

    private var books: MutableList<Book>
    private val bookListener: BookInteractionListener

    init {
        books = mutableListOf()
        this.bookListener = bookListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): BookHolder {
        val itemView: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_book, parent, false)
        return BookHolder(itemView)
    }

    override fun getItemCount(): Int = books.size

    override fun onBindViewHolder(holder: BookHolder, position: Int) {
        holder.bind(books[position], bookListener)
    }


    fun setBooks(books: MutableList<Book>) {
        this.books = books
        notifyDataSetChanged()
    }

    fun getBookByPosition(position: Int): Book {
        return books[position]
    }

    fun removeAt(position: Int) {
        books.removeAt(position)
        notifyItemRemoved(position)
    }

    fun getNewPositionOfNewBook(book: Book): Int {
        books.forEach {
            if (book.yearOfPublish >= it.yearOfPublish) {
                return books.indexOf(it)
            }
        }
        return books.lastIndex + 1
    }

    fun addAt(position: Int, book: Book) {
        books.add(position, book)
        notifyItemInserted(position)
    }

    fun getBookById(id: Int): Book? {
        books.forEach {
            if (it.id == id) return it
        }
        return null
    }

}

class BookHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(book: Book, bookListener: BookInteractionListener) {
        itemView.titleView.text = book.title
        itemView.authorView.text = book.author
        itemView.publishedDateView.text = book.yearOfPublish.toString()

        itemView.setOnClickListener { bookListener.onClick(book) }
    }
}

abstract class SwipeToDeleteCallback : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

    // We don't want to move up and down items
    override fun onMove(p0: RecyclerView, p1: RecyclerView.ViewHolder, p2: RecyclerView.ViewHolder): Boolean {
        return false
    }
}