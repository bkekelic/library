package bkekelic.library.view.adapters

import bkekelic.library.model.Book

interface BookInteractionListener {
    fun onClick(book: Book)
}