package bkekelic.library.viewModel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.content.Intent
import bkekelic.library.BookInfoActivity
import bkekelic.library.model.Book
import bkekelic.library.repository.BookRepository
import bkekelic.library.view.adapters.BookAdapter
import bkekelic.library.view.adapters.BookInteractionListener
import io.reactivex.Flowable

class BookViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: BookRepository = BookRepository(application)
    private val allBooks: Flowable<List<Book>>
    lateinit var bookAdapter: BookAdapter

    // For item on click
    private val bookListener = object: BookInteractionListener {
        override fun onClick(book: Book) {
            val displayIntent = Intent(application, BookInfoActivity::class.java)
            displayIntent.putExtra(BookInfoActivity.KEY_BOOK_INFO, book)
            displayIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            application.startActivity(displayIntent)
        }
    }

    init {
        bookAdapter = BookAdapter(bookListener)
        allBooks = repository.getAllBooks()
    }

    fun getAllBooks(): Flowable<List<Book>> {
        return allBooks
    }

    fun insertBook(book: Book) {
        repository.insertBook(book)
    }

    fun deleteBook(book: Book) {
        repository.deleteBook(book)
    }

    fun getBookWithMaxId(): Book? {
        return repository.getBookWithMaxId()
    }
}